#!/usr/bin/env bash

#set up database
Rscript -e 'source("./funs.R"); createAirDatabase("./data/air.sqlite")'

#run app
Rscript -e 'app <- plumber::plumb("./api.R"); app$run(host="0.0.0.0", port=8000, swagger=T)'

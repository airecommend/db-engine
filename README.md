db engine is the main storage gatherer for interaction data. for each tenant there is at lease one storage engine, and in case a tenant has a test and prod environments, or different use-case models, there is one such engine per usse case. the db engine sits on 80001 port.

- build docker
`docker build . -t 'db'`

- make a volume to store database
`docker volume create air`

- start up the db engine 
`docker run --rm -d -v air:/data -p 8001:8001 db`

- call data gathering service
`curl -H "Content-Type: application/json" -X POST -d '{"items":["beer","diapers","chips"], "user":"father"}' http:/127.0.0.1:8001/recordInteraction`

- import initial dataset (tested up to 100K rows)
`curl -H "Content-Type: application/json" -X POST -d @dat.json http:/127.0.0.1:8001/recordInteraction`

- check whats items there are in database 
`curl -H "Content-Type: application/json" -X GET http://127.0.0.1:8001/listItems`

- check how many items there are
`curl -H "Content-Type: application/json" -X GET http://127.0.0.1:8001/listItemCount`

- check how many users there are
`curl -H "Content-Type: application/json" -X GET http://127.0.0.1:8001/listUserCount`

